package michalrys.schoolscreen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolscreenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchoolscreenApplication.class, args);
    }

}
